

const slugger = (...args) => {
    return args.join(' ').split(' ').filter(str => str!=='').join('-');
}

export default slugger;

const input = ['Slugger works','    as  ', 'expected'];
        const output = 'Slugger-works-as-expected';
        console.log(input.join(' ').split(' ').filter(str => str!=='').join('-'));
