
import slugger from './index.js'
/**
 * @describe [optional] - group of tests with a header to describe them
 */
 describe('testing slugger basic functionality', () => {
    /**
     * @it - unit tests can use the 'it' syntax
     */
    it('slugger can slug string with spaces', () => {
        const input = ['Slugger works'];
        const output = 'Slugger-works';
        expect(slugger(...input)).toEqual(output);
    })
    /**
     * @test - unit test can use the 'test' syntax
     */
    test('slugger can slug any number of spacy strings', () => {
        const input = ['Slugger works','    as  ', 'expected'];
        const output = 'Slugger-works-as-expected';
        expect(slugger(...input)).toEqual(output);
    })
})